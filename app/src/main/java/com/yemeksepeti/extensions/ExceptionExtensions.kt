package com.yemeksepeti.extensions


fun Throwable?.optMessage(): String {
    return if (this is Exception && !this.message.isNullOrEmpty()) {
        this.message!!
    } else {
        "Bir hata oluştu,lütfen tekrar deneyiniz!"
    }
}