package com.yemeksepeti.extensions

import com.yemeksepeti.view.base.BaseViewable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

inline fun <reified T> Single<T>.subscribe(
    viewable: BaseViewable,
    crossinline onSuccess: (T) -> Unit
): Disposable {
    return subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe({
        viewable.dismissProgressDialog()
        onSuccess(it)
    }, {
        viewable.dismissProgressDialog()
        viewable.showError(it.optMessage())
    })
}