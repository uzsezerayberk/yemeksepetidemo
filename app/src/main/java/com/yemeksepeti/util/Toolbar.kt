package com.yemeksepeti.util

import android.app.Activity
import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import com.yemeksepeti.view.R

class Toolbar @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
    FrameLayout(context, attrs, defStyleAttr) {

    private val textViewToolbarTitle: TextView by bindView(R.id.layoutToolbar_title)
    private val imageViewLeftIcon: View by bindView(R.id.layoutToolbar_imageView_leftIcon)

    init {
        View.inflate(context, R.layout.layout_toolbar, this)

        val typedArray: TypedArray = context.theme.obtainStyledAttributes(attrs, R.styleable.YemekSepetiToolbar, 0, 0)
        val titleText: String = typedArray.getString(R.styleable.YemekSepetiToolbar_toolbar_title) ?: "Yemek Sepeti"
        val hideLeftIcon: Boolean = typedArray.getBoolean(R.styleable.YemekSepetiToolbar_toolbar_hideLeftIcon, false)

        textViewToolbarTitle.text = titleText
        imageViewLeftIcon.setOnClickListener { onBackIconClick() }

        if (hideLeftIcon) {
            imageViewLeftIcon.visibility = View.GONE
        }

    }


    fun setToolbarTitle(title: String) {
        textViewToolbarTitle.text = title
    }

    private fun onBackIconClick() {
        if (context is Activity) {
            (context as Activity).onBackPressed()
        }
    }

}