package com.yemeksepeti.util

import android.content.Context
import android.content.res.TypedArray
import android.text.InputType
import android.util.AttributeSet
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.yemeksepeti.extensions.onTextChange
import com.yemeksepeti.view.R

class FormView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
    LinearLayout(context, attrs, defStyleAttr) {


    private val textViewLabel: TextView by bindView(R.id.layoutFormView_textView_label)
    private val editTextValue: EditText by bindView(R.id.layoutFormView_editText_value)

    init {
        val typedArray: TypedArray? =
            attrs?.let { context.theme.obtainStyledAttributes(attrs, R.styleable.FormView, 0, 0) }
        val label: String = typedArray?.getString(R.styleable.FormView_formView_label)
            ?: "label"
        val value: String = typedArray?.getString(R.styleable.FormView_formView_value)
            ?: "value"
        val isInputTypeNumber = typedArray?.getBoolean(R.styleable.FormView_formView_inputTypeNumber, false) ?: false

        val isEditable: Boolean = typedArray?.getBoolean(R.styleable.FormView_formView_isEditable, true) ?: true

        View.inflate(context, R.layout.layout_form_view, this)


        textViewLabel.text = label
        editTextValue.setText(value)
        editTextValue.isFocusable = isEditable
        editTextValue.alpha = if (isEditable) {
            1f
        } else {
            0.6f
        }

        if (isInputTypeNumber) {
            editTextValue.inputType = InputType.TYPE_CLASS_NUMBER
        }

    }

    fun onTextChange(watcher: (CharSequence?) -> Unit) {
        editTextValue.onTextChange(watcher)
    }

    fun setValue(value: String) {
        editTextValue.setText(value)
    }


}

