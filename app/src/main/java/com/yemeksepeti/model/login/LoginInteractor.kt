package com.yemeksepeti.model.login

import io.reactivex.Single
import org.parceler.Parcel
import org.parceler.ParcelConstructor
import org.parceler.ParcelProperty

class LoginInteractor {

    fun login(userName: String, password: String): Single<LoginResponse> {
        return Single.fromCallable {
            dummyLoginResponse
        }
    }


    companion object {
         val dummyLoginResponse = LoginResponse(
            loginMessage = "",
            lastLoginFailDate = "05.03.2019",
            lastLoginSuccessDate = "08.03.2019",
            user = User(
                userId = "124124924",
                name = "Ayberk"
            )
        )
    }
}

data class LoginResponse(
    val loginMessage: String,
    val lastLoginFailDate: String,
    val lastLoginSuccessDate: String,
    val user: User
)


@Parcel(Parcel.Serialization.BEAN)
data class User @ParcelConstructor constructor(
    @ParcelProperty("userId") val userId: String,
    @ParcelProperty("name") val name: String
)