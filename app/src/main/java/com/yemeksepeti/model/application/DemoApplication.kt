package com.yemeksepeti.model.application

import android.app.Application
import com.yemeksepeti.model.core.Core

class DemoApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Core.init(applicationContext)
    }
}