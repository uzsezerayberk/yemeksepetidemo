package com.yemeksepeti.model.core

import android.content.Context
import com.yemeksepeti.model.AppScope
import dagger.Component
import dagger.Module
import dagger.Provides


@AppScope
@Component(modules = [(AppModule::class)])
interface AppComponent {
    fun getContext(): Context
}

interface BaseAppComponentBuilder<out T, out K> {
    fun appComponent(appComponent: AppComponent): T
    fun build(): K
}


@Module
class AppModule(private val context: Context) {

    @Provides
    @AppScope
    fun provideContext() = context
}