package com.yemeksepeti.model.core

import android.annotation.SuppressLint
import android.content.Context

@SuppressLint("StaticFieldLeak")
object Core {

    private var context: Context? = null

    private var appComponent: AppComponent? = null
        get() = field ?: DaggerAppComponent.builder()
            .appModule(AppModule(context!!))
            .build()
            .also { field = it }


    fun init(context: Context) {
        this.context = context.applicationContext
    }

    fun appComponent() = appComponent!!
    fun context() = context!!
}