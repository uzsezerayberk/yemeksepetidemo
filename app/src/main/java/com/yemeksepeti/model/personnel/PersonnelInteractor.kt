package com.yemeksepeti.model.personnel

import io.reactivex.Single

class PersonnelInteractor {

    fun getPersonnelList(adminId: String): Single<List<Personnel>> {
        return Single.fromCallable {
            dummyPersonnelListResponse.toList()
        }
    }

    fun getPersonnelDetail(personnelId: String): Single<PersonnelDetail> {
        return Single.fromCallable {
            dummyPersonnelDetailList.find { it.personnelId == personnelId }
        }
    }

    fun updatePersonnelInfo(mail: String, address: String, phoneNumber: String): Single<String> {
        return Single.fromCallable {
            "Your profile has been successfully updated!"
        }
    }


    companion object {
        val dummyPersonnelListResponse = mutableListOf<Personnel>().apply {
            add(
                Personnel(
                    "10000",
                    "Sebastiano",
                    "Poggi",
                    "https://miro.medium.com/fit/c/240/240/1*LHyJjkJSyZ6aObwngD-HZg.png"
                )
            )
            add(
                Personnel(
                    "10001",
                    "Robert",
                    "Gardner",
                    "https://media.licdn.com/dms/image/C5603AQH7Oj6GjEg61A/profile-displayphoto-shrink_200_200/0?e=1557360000&v=beta&t=ddxjfyPsDE6VLmepNg5ktFgG9ztOVMEdH80-qLfr1og"
                )
            )
            add(
                Personnel(
                    "10002",
                    "Yuki",
                    "Anzai",
                    "https://lh3.googleusercontent.com/a-/AAuE7mAykRekeOGwZ2EEZvUdALiz8tjjSze-FIgdbWLnPA=s640-rw-il"
                )
            )
            add(
                Personnel(
                    "10003",
                    "Elif",
                    "Boncuk",
                    "https://miro.medium.com/max/2400/1*ahnGBaFUtd1HDEAoLJqdIw.jpeg"
                )
            )
            add(
                Personnel(
                    "10004",
                    "Michael",
                    "Wolfson",
                    "https://miro.medium.com/max/2400/0*70yYjDWQlfL9WjoD.jpg"
                )
            )
            add(
                Personnel(
                    "10005",
                    "Andrew",
                    "Kelly",
                    "https://lh4.googleusercontent.com/-t78KEt8AEOg/AAAAAAAAAAI/AAAAAAAAACY/-lyW1IUpLpU/photo.jpg"
                )
            )
            add(
                Personnel(
                    "10006", "Nelson", "Glauber",
                    "https://pbs.twimg.com/profile_images/836560780422164480/vuClsC2w_400x400.jpg"
                )
            )
            add(
                Personnel(
                    "10007",
                    "Daniel",
                    "Lew",
                    "https://blog.trello.com/hs-fs/hubfs/Imported_Blog_Media/pic-199x300.jpg?width=398&height=600&name=pic-199x300.jpg"
                )
            )
            add(
                Personnel(
                    "10008",
                    "Etienne",
                    "Caron",
                    "https://pbs.twimg.com/profile_images/573125347568312321/MYcqe5hf_400x400.jpeg"
                )
            )
            add(
                Personnel(
                    "10009",
                    "Mateusz",
                    "Herych",
                    "https://dfua15.firebaseapp.com/images/people/mateus_herych.jpg"
                )
            )
            add(
                Personnel(
                    "10010",
                    "Huyen",
                    "Tue Dao",
                    "https://pbs.twimg.com/profile_images/720468262434316288/nn98feDK_400x400.jpg"
                )
            )
            add(
                Personnel(
                    "10011",
                    "Rebecca",
                    "Franks",
                    "https://miro.medium.com/fit/c/240/240/1*aYhLOZvMavFWWysj3G_2SQ.png"
                )
            )
            add(
                Personnel(
                    "10012",
                    "Cyril",
                    "Mottier",
                    "https://pbs.twimg.com/profile_images/425559011409158144/yrccrk4F_400x400.jpeg"
                )
            )
            add(
                Personnel(
                    "10013",
                    "Zarah",
                    "Dominguez",
                    "https://pbs.twimg.com/profile_images/790815005901922304/mBP_2hRs_400x400.jpg"
                )
            )
            add(
                Personnel(
                    "10014",
                    "Corey",
                    "Latislaw",
                    "https://avatars0.githubusercontent.com/u/571113?s=460&v=4"
                )
            )
            add(
                Personnel(
                    "10015",
                    "Ryan",
                    "Harter",
                    "https://pbs.twimg.com/profile_images/941371577257533440/m7wK6rMK_400x400.jpg"
                )
            )
            add(
                Personnel(
                    "10016",
                    "Larry",
                    "Schiefer",
                    "https://miro.medium.com/fit/c/240/240/1*gk8TKU7ptB_oFSUBFl1GoA.jpeg"
                )
            )
            add(
                Personnel(
                    "10017",
                    "David",
                    "Gonzalez",
                    "https://secure.meetupstatic.com/photos/member/f/d/7/member_279484055.jpeg"
                )
            )
        }

        val dummyPersonnelDetailList = mutableListOf<PersonnelDetail>().apply {
            add(
                PersonnelDetail(
                    "10000",
                    "Sebastiano",
                    "Poggi",
                    "https://miro.medium.com/fit/c/240/240/1*LHyJjkJSyZ6aObwngD-HZg.png",
                    "02.09.1988",
                    "sebastianopoggi@google.com",
                    "Sebastiano Poggi Address Info",
                    "+1-541-754-3010"
                )
            )
            add(
                PersonnelDetail(
                    "10001",
                    "Robert",
                    "Gardner",
                    "https://media.licdn.com/dms/image/C5603AQH7Oj6GjEg61A/profile-displayphoto-shrink_200_200/0?e=1557360000&v=beta&t=ddxjfyPsDE6VLmepNg5ktFgG9ztOVMEdH80-qLfr1og",
                    "12.12.1990",
                    "robertgardner@google.com",
                    "Robert Gardner Address Info",
                    "+1-541-754-3011"
                )
            )
            add(
                PersonnelDetail(
                    "10002",
                    "Yuki",
                    "Anzai",
                    "https://lh3.googleusercontent.com/a-/AAuE7mAykRekeOGwZ2EEZvUdALiz8tjjSze-FIgdbWLnPA=s640-rw-il",
                    "2.03.1989",
                    "yukianzai@google.com",
                    "Yuki Anzai Address Info",
                    "+1-541-754-3012"
                )
            )
            add(
                PersonnelDetail(
                    "10003",
                    "Elif",
                    "Boncuk",
                    "https://miro.medium.com/max/2400/1*ahnGBaFUtd1HDEAoLJqdIw.jpeg",
                    "01.05.1985",
                    "elifboncuk@google.com",
                    "Elif Boncuk Address Info",
                    "+1-541-754-3013"
                )
            )
            add(
                PersonnelDetail(
                    "10004",
                    "Michael",
                    "Wolfson",
                    "https://miro.medium.com/max/2400/0*70yYjDWQlfL9WjoD.jpg",
                    "11.11.1991",
                    "michaelwolfson@google.com",
                    "Michael Wolfson Address Info",
                    "+1-541-754-3014"
                )
            )
            add(
                PersonnelDetail(
                    "10005",
                    "Andrew",
                    "Kelly",
                    "https://lh4.googleusercontent.com/-t78KEt8AEOg/AAAAAAAAAAI/AAAAAAAAACY/-lyW1IUpLpU/photo.jpg",
                    "05.05.1988",
                    "andrewkelly@google.com",
                    "Andrew Kelly Address Info",
                    "+1-541-754-3015"
                )
            )
            add(
                PersonnelDetail(
                    "10006",
                    "Nelson",
                    "Glauber",
                    "https://pbs.twimg.com/profile_images/836560780422164480/vuClsC2w_400x400.jpg",
                    "11.09.1983",
                    "nelsongaluber@google.com",
                    "Nelson Glauber Address Info",
                    "+1-541-754-3016"
                )
            )
            add(
                PersonnelDetail(
                    "10007",
                    "Daniel",
                    "Lew",
                    "https://blog.trello.com/hs-fs/hubfs/Imported_Blog_Media/pic-199x300.jpg?width=398&height=600&name=pic-199x300.jpg",
                    "21.03.1991",
                    "sebastianopoggi@google.com",
                    "Daniel Lew Address Info",
                    "+1-541-754-3017"
                )
            )
            add(
                PersonnelDetail(
                    "10008",
                    "Etienne",
                    "Caron",
                    "https://pbs.twimg.com/profile_images/573125347568312321/MYcqe5hf_400x400.jpeg",
                    "20.09.1980",
                    "etiennecaron@google.com",
                    "Etienne Caron Address Info",
                    "+1-541-754-3018"
                )
            )
            add(
                PersonnelDetail(
                    "10009",
                    "Mateusz",
                    "Herych",
                    "https://dfua15.firebaseapp.com/images/people/mateus_herych.jpg",
                    "11.01.1985",
                    "mateuszherych@google.com",
                    "Mateusz Herych Address Info",
                    "+1-541-754-3019"
                )
            )
            add(
                PersonnelDetail(
                    "10010",
                    "Huyen",
                    "Tue Dao",
                    "https://pbs.twimg.com/profile_images/720468262434316288/nn98feDK_400x400.jpg",
                    "07.05.1987",
                    "sebastianopoggi@google.com",
                    "Huyen Tue Dao Address Info",
                    "+1-541-754-3020"
                )
            )
            add(
                PersonnelDetail(
                    "10011",
                    "Rebecca",
                    "Franks",
                    "https://miro.medium.com/fit/c/240/240/1*aYhLOZvMavFWWysj3G_2SQ.png",
                    "03.03.1983",
                    "rebeccafranks@google.com",
                    "Rebecca Franks Address Info",
                    "+1-541-754-3021"
                )
            )
            add(
                PersonnelDetail(
                    "10012",
                    "Cyril",
                    "Mottier",
                    "https://pbs.twimg.com/profile_images/425559011409158144/yrccrk4F_400x400.jpeg",
                    "12.02.1990",
                    "cyrilmottier@google.com",
                    "Cyril Mottier Address Info",
                    "+1-541-754-3022"
                )
            )
            add(
                PersonnelDetail(
                    "10013",
                    "Zarah",
                    "Dominguez",
                    "https://pbs.twimg.com/profile_images/790815005901922304/mBP_2hRs_400x400.jpg",
                    "02.01.1981",
                    "zarahdominguez@google.com",
                    "Zarah Dominguez Address Info",
                    "+1-541-754-3023"
                )
            )
            add(
                PersonnelDetail(
                    "10014",
                    "Corey",
                    "Latislaw",
                    "https://avatars0.githubusercontent.com/u/571113?s=460&v=4",
                    "22.05.1983",
                    "coreylatislaw@google.com",
                    "Corey Latislaw Address Info",
                    "+1-541-754-3024"
                )
            )
            add(
                PersonnelDetail(
                    "10015",
                    "Ryan",
                    "Harter",
                    "https://pbs.twimg.com/profile_images/941371577257533440/m7wK6rMK_400x400.jpg",
                    "20.11.1988",
                    "ryanharter@google.com",
                    "Ryan Harter Address Info",
                    "+1-541-754-3025"
                )
            )
            add(
                PersonnelDetail(
                    "10016",
                    "Larry",
                    "Schiefer",
                    "https://miro.medium.com/fit/c/240/240/1*gk8TKU7ptB_oFSUBFl1GoA.jpeg",
                    "12.09.1984",
                    "larryschiefer@google.com",
                    "Larry Schiefer Address Info",
                    "+1-541-754-3026"
                )
            )
            add(
                PersonnelDetail(
                    "10017",
                    "David",
                    "Gonzalez",
                    "https://secure.meetupstatic.com/photos/member/f/d/7/member_279484055.jpeg",
                    "11.06.1992",
                    "davidgonzalez@google.com",
                    "David Gonzalez Address Info",
                    "+1-541-754-3027"
                )
            )
        }
    }
}

data class Personnel(
    val personnelId: String,
    val personnelName: String,
    val personnelSurName: String,
    val personnelImage: String
)

data class PersonnelDetail(
    val personnelId: String,
    val personnelName: String,
    val personnelSurName: String,
    val personnelImage: String,
    val personnelBirthDate: String,
    val personnelMail: String,
    val personnelAddress: String,
    val personnelPhoneNumber: String
)