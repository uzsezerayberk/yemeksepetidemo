package com.yemeksepeti.view.personneldetail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.yemeksepeti.model.core.Core
import com.yemeksepeti.util.FormView
import com.yemeksepeti.util.bindView
import com.yemeksepeti.util.getExtra
import com.yemeksepeti.view.R
import com.yemeksepeti.view.base.BaseActivity

class PersonnelDetailActivity : BaseActivity(), PersonnelDetailViewable {

    private val imageViewProfile: ImageView by bindView(R.id.activityPersonnelDetail_imageView_profile)
    private val formViewNameSurname: FormView by bindView(R.id.activityPersonnelDetail_formView_nameSurname)
    private val formViewBirthDate: FormView by bindView(R.id.activityPersonnelDetail_formView_birthDate)
    private val formViewMail: FormView by bindView(R.id.activityPersonnelDetail_formView_mail)
    private val formViewPhoneNumber: FormView by bindView(R.id.activityPersonnelDetail_formView_phoneNumber)
    private val formViewAddress: FormView by bindView(R.id.activityPersonnelDetail_formView_address)
    private val textViewUpdate: TextView by bindView(R.id.activityPersonnelDetail_textView_update)

    private val presenter: PersonnelDetailPresenter by lazy {
        DaggerPersonnelDetailPresenterComponent.builder()
            .viewable(this)
            .personnelId(getExtra(EXTRAS_PERSONNEL_ID))
            .appComponent(Core.appComponent())
            .build()
            .get()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_personnel_detail)

        formViewMail.onTextChange { presenter.onMailChange(it.toString()) }
        formViewPhoneNumber.onTextChange { presenter.onPhoneNumberChange(it.toString()) }
        formViewAddress.onTextChange { presenter.onAddressChange(it.toString()) }
        textViewUpdate.setOnClickListener { presenter.onUpdateClick() }

        presenter.onViewCreate()
    }

    override fun setProfileImage(imageUrl: String) {
        Glide.with(this).load(imageUrl).circleCrop().into(imageViewProfile)
    }

    override fun setNameSurname(nameSurname: String) {
        formViewNameSurname.setValue(nameSurname)
    }

    override fun setBirthDate(birthDate: String) {
        formViewBirthDate.setValue(birthDate)
    }

    override fun setPhoneNumber(phoneNumber: String) {
        formViewPhoneNumber.setValue(phoneNumber)
    }

    override fun setEmail(email: String) {
        formViewMail.setValue(email)
    }

    override fun setAddress(address: String) {
        formViewAddress.setValue(address)
    }

    override fun enableUpdateButton() {
        textViewUpdate.alpha = 1f
        textViewUpdate.isClickable = true
    }

    override fun disableUpdateButton() {
        textViewUpdate.alpha = 0.5f
        textViewUpdate.isClickable = false
    }


    companion object {
        private const val EXTRAS_PERSONNEL_ID = "EXTRAS_PERSONNEL_ID"
        fun getIntent(context: Context, personnelId: String) =
            Intent(context, PersonnelDetailActivity::class.java).apply {
                putExtra(EXTRAS_PERSONNEL_ID, personnelId)
            }
    }
}