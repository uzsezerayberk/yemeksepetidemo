package com.yemeksepeti.view.personneldetail

import android.annotation.SuppressLint
import com.yemeksepeti.extensions.subscribe
import com.yemeksepeti.model.ActivityScope
import com.yemeksepeti.model.core.AppComponent
import com.yemeksepeti.model.core.BaseAppComponentBuilder
import com.yemeksepeti.model.personnel.PersonnelDetail
import com.yemeksepeti.model.personnel.PersonnelInteractor
import com.yemeksepeti.view.base.BaseViewable
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.Provides
import javax.inject.Inject

class PersonnelDetailPresenter @Inject constructor(
    private val viewable: PersonnelDetailViewable,
    private val interactor: PersonnelInteractor,
    private val personnelId: String
) {

    private var mail: String = ""
    private var phoneNumber: String = ""
    private var address: String = ""

    private var personnelInfo: PersonnelDetail? = null

    @SuppressLint("CheckResult")
    fun onViewCreate() {
        viewable.showProgressDialog()
        viewable.disableUpdateButton()
        interactor.getPersonnelDetail(personnelId)
            .doOnSuccess {
                mail = it.personnelMail
                phoneNumber = it.personnelPhoneNumber
                address = it.personnelAddress
                personnelInfo = it
            }
            .subscribe(viewable) { personnelDetail ->
                viewable.setProfileImage(personnelDetail.personnelImage)
                viewable.setNameSurname(personnelDetail.personnelName + " " + personnelDetail.personnelSurName)
                viewable.setBirthDate(personnelDetail.personnelBirthDate)
                viewable.setPhoneNumber(personnelDetail.personnelPhoneNumber)
                viewable.setAddress(personnelDetail.personnelAddress)
                viewable.setEmail(personnelDetail.personnelMail)
            }

    }

    fun onMailChange(mail: String) {
        this.mail = mail
        onInfoChange()
    }

    fun onPhoneNumberChange(phoneNumber: String) {
        this.phoneNumber = phoneNumber
        onInfoChange()
    }

    fun onAddressChange(address: String) {
        this.address = address
        onInfoChange()
    }

    private fun onInfoChange() {
        if (checkPersonnelInfoIsUpdated()) {
            viewable.enableUpdateButton()
        } else {
            viewable.disableUpdateButton()
        }
    }

    private fun checkPersonnelInfoIsUpdated(): Boolean {
        return if (personnelInfo == null) {
            false
        } else {
            !(personnelInfo?.personnelAddress == address && personnelInfo?.personnelPhoneNumber == phoneNumber && personnelInfo?.personnelMail == mail)
        }
    }

    fun onUpdateClick() {
        viewable.showProgressDialog()
        interactor.updatePersonnelInfo(mail, address, phoneNumber).subscribe(viewable){
            viewable.showMessage(it)
        }
    }

}


interface PersonnelDetailViewable : BaseViewable {
    fun setProfileImage(imageUrl: String)
    fun setNameSurname(nameSurname: String)
    fun setBirthDate(birthDate: String)
    fun setPhoneNumber(phoneNumber: String)
    fun setEmail(email: String)
    fun setAddress(address: String)
    fun enableUpdateButton()
    fun disableUpdateButton()
}

@Module
class PersonnelDetailPresenterModule {
    @Provides
    fun personnelInteractor() = PersonnelInteractor()
}

@ActivityScope
@Component(dependencies = [AppComponent::class], modules = [PersonnelDetailPresenterModule::class])
interface PersonnelDetailPresenterComponent {
    fun get(): PersonnelDetailPresenter

    @Component.Builder
    interface Builder : BaseAppComponentBuilder<Builder, PersonnelDetailPresenterComponent> {
        @BindsInstance
        fun viewable(viewable: PersonnelDetailViewable): Builder

        @BindsInstance
        fun personnelId(personnelId: String): Builder
    }
}