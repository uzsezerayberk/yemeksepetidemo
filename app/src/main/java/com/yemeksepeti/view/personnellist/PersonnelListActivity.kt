package com.yemeksepeti.view.personnellist

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.yemeksepeti.model.core.Core
import com.yemeksepeti.model.login.User
import com.yemeksepeti.model.personnel.Personnel
import com.yemeksepeti.util.bindView
import com.yemeksepeti.util.getExtra
import com.yemeksepeti.view.R
import com.yemeksepeti.view.base.BaseActivity
import com.yemeksepeti.view.personneldetail.PersonnelDetailActivity
import org.parceler.Parcels

class PersonnelListActivity : BaseActivity(), PersonnelListViewable {

    private val recyclerView: RecyclerView by bindView(R.id.activityPersonnelList_recyclerView)

    private val adapter: PersonnelListAdapter by lazy {
        PersonnelListAdapter(
            onItemClickListener = { personnel ->
                presenter.onPersonnelClick(personnel)
            }
        )
    }

    private val presenter: PersonnelListPresenter by lazy {
        DaggerPersonnelListPresenterComponent.builder()
            .loginUser(Parcels.unwrap(getExtra(EXTRAS_USER)))
            .viewable(this)
            .appComponent(Core.appComponent())
            .build()
            .get()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_personnel_list)

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

        presenter.onViewCreate()
    }

    override fun updateData(personnelList: List<Personnel>) {
        adapter.updateList(personnelList)
    }

    override fun redirectToPersonnelDetail(personnelId: String) {
        startActivity(PersonnelDetailActivity.getIntent(this, personnelId))
    }


    companion object {
        private const val EXTRAS_USER = "EXTRAS_USER"
        fun getIntent(context: Context, user: User) = Intent(context, PersonnelListActivity::class.java).apply {
            putExtra(EXTRAS_USER, Parcels.wrap(User::class.java, user))
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }
    }
}