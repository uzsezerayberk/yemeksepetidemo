package com.yemeksepeti.view.personnellist

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.yemeksepeti.model.personnel.Personnel
import com.yemeksepeti.util.bindView
import com.yemeksepeti.view.R

class PersonnelListAdapter(private val onItemClickListener: (personnel: Personnel) -> Unit) :
    RecyclerView.Adapter<PersonnelListViewHolder>() {

    private var itemList = mutableListOf<Personnel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = PersonnelListViewHolder(parent).apply {
        itemView.setOnClickListener {
            onItemClickListener.invoke(itemList[adapterPosition])
        }
    }

    override fun getItemCount() = itemList.size

    override fun onBindViewHolder(holder: PersonnelListViewHolder, position: Int) {
        holder.bind(itemList[position])
    }

    fun updateList(personnelList: List<Personnel>) {
        itemList.clear()
        itemList.addAll(personnelList)
        notifyDataSetChanged()
    }

}


class PersonnelListViewHolder(parent: ViewGroup) :
    RecyclerView.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.row_personnel_list, parent, false)) {

    private val textViewName: TextView by bindView(R.id.rowPersonnelList_textView_name)
    private val textViewSurname: TextView by bindView(R.id.rowPersonnelList_textView_surname)
    private val imageViewImage: ImageView by bindView(R.id.rowPersonnelList_imageView_image)


    fun bind(personnel: Personnel) {
        textViewName.text = personnel.personnelName
        textViewSurname.text = personnel.personnelSurName
        Glide.with(itemView).load(personnel.personnelImage).circleCrop().into(imageViewImage)
    }

}


