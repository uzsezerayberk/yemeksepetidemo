package com.yemeksepeti.view.personnellist

import android.annotation.SuppressLint
import com.yemeksepeti.extensions.subscribe
import com.yemeksepeti.model.ActivityScope
import com.yemeksepeti.model.core.AppComponent
import com.yemeksepeti.model.core.BaseAppComponentBuilder
import com.yemeksepeti.model.login.User
import com.yemeksepeti.model.personnel.Personnel
import com.yemeksepeti.model.personnel.PersonnelInteractor
import com.yemeksepeti.view.base.BaseViewable
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.Provides
import javax.inject.Inject

class PersonnelListPresenter @Inject constructor(
    private val viewable: PersonnelListViewable,
    private val loginUser: User,
    private val interactor: PersonnelInteractor
) {

    @SuppressLint("CheckResult")
    fun onViewCreate() {
        viewable.showProgressDialog()
        interactor.getPersonnelList(loginUser.userId)
            .subscribe(viewable) {
                viewable.updateData(it)
            }

    }

    fun onPersonnelClick(personnel: Personnel) {
        viewable.redirectToPersonnelDetail(personnel.personnelId)
    }
}


interface PersonnelListViewable : BaseViewable {
    fun updateData(personnelList: List<Personnel>)
    fun redirectToPersonnelDetail(personnelId: String)
}

@Module
class PersonnelListPresenterModule {
    @Provides
    fun personnelListInteractor() = PersonnelInteractor()
}

@ActivityScope
@Component(dependencies = [AppComponent::class], modules = [PersonnelListPresenterModule::class])
interface PersonnelListPresenterComponent {
    fun get(): PersonnelListPresenter

    @Component.Builder
    interface Builder : BaseAppComponentBuilder<Builder, PersonnelListPresenterComponent> {
        @BindsInstance
        fun viewable(viewable: PersonnelListViewable): Builder

        @BindsInstance
        fun loginUser(user: User): Builder
    }
}