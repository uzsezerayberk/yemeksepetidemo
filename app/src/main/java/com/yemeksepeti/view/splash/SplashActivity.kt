package com.yemeksepeti.view.splash

import android.os.Bundle
import android.os.Handler
import com.yemeksepeti.view.R
import com.yemeksepeti.view.base.BaseActivity
import com.yemeksepeti.view.login.LoginActivity

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({
           startActivity(LoginActivity.getIntent(this))
        },1500)


    }
}
