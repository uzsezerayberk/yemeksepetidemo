package com.yemeksepeti.view.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.EditText
import android.widget.TextView
import com.yemeksepeti.extensions.onTextChange
import com.yemeksepeti.model.core.Core
import com.yemeksepeti.model.login.User
import com.yemeksepeti.util.bindView
import com.yemeksepeti.view.R
import com.yemeksepeti.view.base.BaseActivity
import com.yemeksepeti.view.personnellist.PersonnelListActivity

class LoginActivity : BaseActivity(), LoginViewable {

    private val editTextUser: EditText by bindView(R.id.activityLogin_editText_user)
    private val editTextPassword: EditText by bindView(R.id.activityLogin_editText_password)
    private val textViewLogin: TextView by bindView(R.id.activityLogin_textView_login)

    private val presenter: LoginPresenter by lazy {
        DaggerLoginPresenterComponent.builder()
            .viewable(this)
            .appComponent(Core.appComponent())
            .build()
            .get()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        editTextUser.onTextChange { userName -> presenter.onUserNameTextChange(userName.toString()) }
        editTextPassword.onTextChange { password -> presenter.onPasswordTextChange(password.toString()) }
        textViewLogin.setOnClickListener { presenter.onLoginButtonClick() }

        presenter.onViewCreate()
    }

    override fun enableSignInButton() {
        textViewLogin.alpha = 1f
        textViewLogin.isClickable = true
    }

    override fun disableSignInButton() {
        textViewLogin.alpha = 0.5f
        textViewLogin.isClickable = false
    }

    override fun redirectToUserListPage(user: User) {
        startActivity(PersonnelListActivity.getIntent(this, user))
    }


    companion object {
        fun getIntent(context : Context) = Intent(context,LoginActivity::class.java)
    }

}