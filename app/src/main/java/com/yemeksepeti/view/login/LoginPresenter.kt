package com.yemeksepeti.view.login

import android.annotation.SuppressLint
import com.yemeksepeti.extensions.subscribe
import com.yemeksepeti.model.ActivityScope
import com.yemeksepeti.model.core.AppComponent
import com.yemeksepeti.model.core.BaseAppComponentBuilder
import com.yemeksepeti.model.login.LoginInteractor
import com.yemeksepeti.model.login.User
import com.yemeksepeti.view.base.BaseViewable
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.Provides
import javax.inject.Inject

class LoginPresenter @Inject constructor(
    private val viewable: LoginViewable,
    private val loginInteractor: LoginInteractor
) {

    private var userName = ""
    private var password = ""

    fun onViewCreate() {
        viewable.disableSignInButton()
    }

    fun onUserNameTextChange(userName: String) {
        this.userName = userName
        onInputTextChanged()
    }

    fun onPasswordTextChange(password: String) {
        this.password = password
        onInputTextChanged()
    }

    private fun onInputTextChanged() {
        if (checkValidation()) {
            viewable.enableSignInButton()
        } else {
            viewable.disableSignInButton()
        }
    }

    private fun checkValidation(): Boolean {
        return (userName.isNotEmpty() && password.isNotEmpty())
    }

    @SuppressLint("CheckResult")
    fun onLoginButtonClick() {
        viewable.showProgressDialog()
        loginInteractor.login(userName, password)
            .subscribe(viewable) {
                viewable.redirectToUserListPage(it.user)
            }

    }

}

interface LoginViewable : BaseViewable {
    fun redirectToUserListPage(user: User)
    fun enableSignInButton()
    fun disableSignInButton()
}

@Module
class LoginPresenterModule {
    @Provides
    fun loginInteractor() = LoginInteractor()
}

@ActivityScope
@Component(dependencies = [AppComponent::class], modules = [LoginPresenterModule::class])
interface LoginPresenterComponent {
    fun get(): LoginPresenter

    @Component.Builder
    interface Builder : BaseAppComponentBuilder<Builder, LoginPresenterComponent> {
        @BindsInstance
        fun viewable(loginViewable: LoginViewable): Builder
    }
}