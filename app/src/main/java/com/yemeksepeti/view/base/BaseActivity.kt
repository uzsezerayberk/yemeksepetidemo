package com.yemeksepeti.view.base

import android.app.ProgressDialog
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity() {

    private val progressDialog by lazy {
        ProgressDialog(this).apply {
            setMessage("Loading...")
        }
    }

    open fun showProgressDialog() {
        progressDialog.show()
    }

    open fun dismissProgressDialog() {
        progressDialog.dismiss()
    }

    open fun showMessage(message: String) {
        AlertDialog.Builder(this).setTitle("YemekSepeti").setMessage(message).setPositiveButton("Close", null).show()
    }

    open fun showError(errorMessage: String) {
        showMessage(errorMessage)
    }
}