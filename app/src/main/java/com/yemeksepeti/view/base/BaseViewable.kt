package com.yemeksepeti.view.base

interface BaseViewable{
    fun showProgressDialog()
    fun dismissProgressDialog()
    fun showMessage(message :String)
    fun showError(errorMessage :String)
}