package com.yemeksepeti.view.personneldetail

import com.nhaarman.mockito_kotlin.*
import com.yemeksepeti.model.personnel.PersonnelInteractor
import com.yemeksepeti.utilities.setupTestClass
import com.yemeksepeti.utilities.tearDownTestClass
import io.reactivex.Single
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test
import java.lang.Exception

class PersonnelDetailPresenterTest {

    private val mockViewable = mock<PersonnelDetailViewable>()

    @Test
    fun testOnViewCreate() {
        val mockPersonnelId = "10000"

        val mockInteractor = mock<PersonnelInteractor> {
            on { getPersonnelDetail(any()) }.then { Single.fromCallable { PersonnelInteractor.dummyPersonnelDetailList.find { it.personnelId == mockPersonnelId } } }
        }


        val mockPresenter = PersonnelDetailPresenter(mockViewable, mockInteractor, mockPersonnelId)

        mockPresenter.onViewCreate()
        verify(mockInteractor).getPersonnelDetail(mockPersonnelId)
        inOrder(mockViewable) {
            verify(mockViewable).showProgressDialog()
            verify(mockViewable).disableUpdateButton()
            verify(mockViewable).dismissProgressDialog()
            verify(mockViewable).setProfileImage("https://developers.google.com/experts/img/user/106129364465361264599.png")
            verify(mockViewable).setNameSurname("Sebastiano Poggi")
            verify(mockViewable).setBirthDate("02.09.1988")
            verify(mockViewable).setPhoneNumber("+1-541-754-3010")
            verify(mockViewable).setAddress("Sebastiano Poggi Address Info")
            verify(mockViewable).setEmail("sebastianopoggi@google.com")
        }
    }

    @Test
    fun testGetPersonnelDetailFail() {

        val mockPersonnelId = "10000"

        val mockInteractor = mock<PersonnelInteractor> {
            on { getPersonnelDetail(any()) }.thenReturn(Single.error(Exception("Error Message")))
        }


        val mockPresenter = PersonnelDetailPresenter(mockViewable, mockInteractor, mockPersonnelId)

        mockPresenter.onViewCreate()
        verify(mockInteractor).getPersonnelDetail(mockPersonnelId)
        inOrder(mockViewable) {
            verify(mockViewable).showProgressDialog()
            verify(mockViewable).disableUpdateButton()
            verify(mockViewable).dismissProgressDialog()
            verify(mockViewable).showError("Error Message")

        }
    }

    @Test
    fun testOnUpdateClick() {
        val mockPersonnelId = "10000"


        val mockInteractor = mock<PersonnelInteractor> {
            on { updatePersonnelInfo(anyOrNull(), anyOrNull(), anyOrNull()) }.then {
                Single.fromCallable {
                    "Success Message"
                }
            }
        }
        val mockPresenter = PersonnelDetailPresenter(mockViewable, mockInteractor, mockPersonnelId)
        mockPresenter.onUpdateClick()
        verify(mockInteractor).updatePersonnelInfo(anyOrNull(), anyOrNull(), anyOrNull())
        inOrder(mockViewable) {
            verify(mockViewable).showProgressDialog()
            verify(mockViewable).dismissProgressDialog()
            verify(mockViewable).showMessage("Success Message")
        }

    }


    companion object {
        @BeforeClass
        @JvmStatic
        fun setUpClass() {
            setupTestClass()
        }

        @AfterClass
        @JvmStatic
        fun tearDownClass() {
            tearDownTestClass()
        }
    }

}