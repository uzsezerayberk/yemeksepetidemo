package com.yemeksepeti.view.login

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.inOrder
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.yemeksepeti.model.login.LoginInteractor
import com.yemeksepeti.utilities.setupTestClass
import com.yemeksepeti.utilities.tearDownTestClass
import io.reactivex.Single
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test

class LoginPresenterTest {

    private val mockViewable = mock<LoginViewable>()
    private val mockInteractor = mock<LoginInteractor> {
        on { login(any(), any()) }.thenReturn(Single.fromCallable {
            LoginInteractor.dummyLoginResponse
        })
    }
    private val presenter = LoginPresenter(mockViewable, mockInteractor)


    @Test
    fun testOnViewCreate() {
        presenter.onViewCreate()
        verify(mockViewable).disableSignInButton()
    }


    @Test
    fun testOnUserNameAndPasswordTextChange() {
        presenter.onUserNameTextChange("userName")
        presenter.onPasswordTextChange("password")
        verify(mockViewable).enableSignInButton()
    }

    @Test
    fun testOnLoginClick() {
        presenter.onLoginButtonClick()
        inOrder(mockViewable) {
            verify(mockViewable).showProgressDialog()
            verify(mockViewable).dismissProgressDialog()
            verify(mockViewable).redirectToUserListPage(LoginInteractor.dummyLoginResponse.user)
        }
    }

    @Test
    fun testOnLoginFail() {
        val mockInteractor = mock<LoginInteractor>(){
            on { login(any(), any()) }.thenReturn(Single.error(Exception( "Error Message")))
        }
        val presenter = LoginPresenter(mockViewable,mockInteractor)

        presenter.onLoginButtonClick()
        inOrder(mockViewable) {
            verify(mockViewable).showProgressDialog()
            verify(mockViewable).dismissProgressDialog()
            verify(mockViewable).showError("Error Message")
        }
    }


    companion object {
        @BeforeClass
        @JvmStatic
        fun setUpClass() {
            setupTestClass()
        }

        @AfterClass
        @JvmStatic
        fun tearDownClass() {
            tearDownTestClass()
        }
    }

}