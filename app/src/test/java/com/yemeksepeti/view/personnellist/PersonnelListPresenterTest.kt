package com.yemeksepeti.view.personnellist

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.inOrder
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.yemeksepeti.model.login.User
import com.yemeksepeti.model.personnel.Personnel
import com.yemeksepeti.model.personnel.PersonnelInteractor
import com.yemeksepeti.utilities.setupTestClass
import com.yemeksepeti.utilities.tearDownTestClass
import io.reactivex.Single
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test

class PersonnelListPresenterTest {

    private val mockViewable = mock<PersonnelListViewable>()


    @Test
    fun testOnViewCreate(){
        val mockInteractor = mock<PersonnelInteractor>{
            on { getPersonnelList(any()) }.then { Single.fromCallable { PersonnelInteractor.dummyPersonnelListResponse } }
        }
        val mockLoginUser = mock<User>(){
            on { userId }.thenReturn("mockUserId")
        }
        val presenter = PersonnelListPresenter(mockViewable,mockLoginUser,mockInteractor)

        presenter.onViewCreate()
        verify(mockInteractor).getPersonnelList("mockUserId")
        inOrder(mockViewable){
            verify(mockViewable).showProgressDialog()
            verify(mockViewable).dismissProgressDialog()
            verify(mockViewable).updateData(PersonnelInteractor.dummyPersonnelListResponse)
        }
    }

    @Test
    fun testGetPersonnelListFail(){
        val mockInteractor = mock<PersonnelInteractor>{
            on { getPersonnelList(any()) }.thenReturn(Single.error(Exception("Error Message")))
        }
        val mockLoginUser = mock<User>(){
            on { userId }.thenReturn("mockUserId")
        }
        val presenter = PersonnelListPresenter(mockViewable,mockLoginUser,mockInteractor)

        presenter.onViewCreate()
        verify(mockInteractor).getPersonnelList("mockUserId")
        inOrder(mockViewable){
            verify(mockViewable).showProgressDialog()
            verify(mockViewable).dismissProgressDialog()
            verify(mockViewable).showError("Error Message")
        }
    }


    @Test
    fun testOnPersonnelClick(){
        val mockInteractor = mock<PersonnelInteractor>()
        val mockLoginUser = mock<User>()

        val mockPersonnel = mock<Personnel>{
            on { personnelId }.thenReturn("personnelId")
        }
        val presenter = PersonnelListPresenter(mockViewable,mockLoginUser,mockInteractor)

        presenter.onPersonnelClick(mockPersonnel)
        inOrder(mockViewable){
            verify(mockViewable).redirectToPersonnelDetail("personnelId")
        }
    }




    companion object {
        @BeforeClass
        @JvmStatic
        fun setUpClass() {
            setupTestClass()
        }

        @AfterClass
        @JvmStatic
        fun tearDownClass() {
            tearDownTestClass()
        }
    }

}